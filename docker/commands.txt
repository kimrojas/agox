# Login to Gitlab registry
docker login registry.gitlab.com

# Build the image. 
docker build -t registry.gitlab.com/agox/agox .

# Push the image to the registry
docker push registry.gitlab.com/agox/agox