Models
================================

Examples of the machine learning models implementented
in AGOX and how they can be used.

First, an introduction to the descriptors implemented in AGOX.
Next, description of the standard and sparse Gaussian Process
Regression models available.


.. toctree::
   :maxdepth: 1

   descriptor
   gpr
   sgpr
